<?php

use App\Http\Controllers\ProductsController;
use App\Http\Controllers\Admin\ProductsController as AdminProductsController;
use App\Http\Controllers\Admin\CategoriesController as AdminCategoriesController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('language')->group(function (){
    Route::get('/', [ProductsController::class,'index'])->name('dashboards');
    Route::get('products/{product}',[ProductsController::class,'show'])->name('products.show');
    Route::prefix('admin')->name('admin.')->group(function (){
        Route::resource('products' , AdminProductsController::class);
        Route::resource('categories', AdminCategoriesController::class)->except('show');
    });
    Auth::routes();
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});

Route::get('language/{locale}',
    [App\Http\Controllers\LanguageSwitcherController::class, 'switcher']
)->name('language.switcher')->where('locale','en|ru');
