<?php
return [
    'name' => 'Имя',
    'price' => 'Цена',
    'picture' => 'Фото',
    'welcome' => 'Добро пожаловать, :name',
    'create_new_products' => 'Добавить новый товар',
    'products_list' => 'Бытовая техника МоскваТех',
    'contacts' => 'Контакты',
    'login' => 'Логин',
    'register' => 'Регистрация',
    'logout' => 'Выйти',
    'action' => 'Действия',
    'edit' => 'Редактировать',
    'delete' => 'Удалить',
    'email' => 'Почта',
    'back' => 'Назад',
    'show_more' => 'Узнать больше',
    'admin' => 'Админ',
    'dashboard' => 'Список продуктов'
];
