<?php

return [
    'name' => 'Name',
    'price' => 'Price',
    'picture' => 'Picture',
    'welcome' => 'Welcome, :name',
    'create_new_products' => 'Create new products',
    'products_list' => 'Products',
    'contacts' => 'Contacts',
    'login' => 'Login',
    'register' => 'Register',
    'logout' => 'Logout',
    'action' => 'Action',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'email' => 'Email',
    'back' => 'Back',
    'show_more' => 'Show more',
    'admin' => 'Admin',
    'dashboard' => 'Dashboard'
];
