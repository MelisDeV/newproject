@extends('layouts.app')

@section('content')
    <br>
    <p>Remote your computer contacts: +79016471994</p>
    <p>Clining company contacts: +79075461734</p>
    <div class="container-xl">
        <h1 class="products-main">@lang('messages.products_list')</h1>
        <div class="form-group">
            <select name="category_id" >
                @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row row-cols-1 row-cols-md-4 g-4">
        @foreach($products as $product)
            <div class="col-products"  >
                <div class="card h-100">
                    @if(!is_null($product->picture))
                <img  height="180px" src="{{asset('/storage/' . $product->picture)}}"
                      class="card-img-top" alt="{{$product->picture}}">
                    @else
                       <div><h3>        NO PICTURE</h3></div>
                        <br>
                    @endif
                <div class="card-body text-center">
                    <h5 class="card-title">{{$product->name}}</h5>
                    <p class="card-text">{{$product->price}} $</p>
                    <p><a href="{{route('products.show', ['product' => $product])}}">@lang('messages.show_more')</a></p>
                </div>
                </div>
            </div>
            <br>
        @endforeach
    </div>
<div>
    {{$products->links('pagination::bootstrap-4')}}
</div>

@endsection
