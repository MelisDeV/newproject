@extends('layouts.app')

@section('content')
    <div class="cardShow" style="width: 500px;">
        <img src="{{asset('/storage/' .$product->picture)}}" alt="{{$product->picture}}}" class="card-img-top" alt="{{$product->picture}}">
        <div class="card-body">
            <h3 class="card-title">{{$product->name}}</h3>
            <h4><b>Цена:</b> {{$product->price}} <b>$</b></h4>
            @if(!is_null($product->category))
                <h4>Категория:{{$product->category->name}}</h4>
            @else
                <h4>No category</h4>
            @endif

            <a href="{{route('dashboards')}}" class="btn btn-primary">@lang('messages.back')</a>
        </div>
    </div>
    <div>
        <h4><b>Comments</b></h4>
    </div>
        <div class="col-8 scrollit">
            @foreach($product->comments as $comment)
                <div class="media g-mb-30 media-comment">
                    <div class="media-body u-shadow-v18 g-bg-secondary g-pa-30">
                        <div class="g-mb-15">
                            <h5 class="h5 g-color-gray-dark-v1 mb-0">{{$comment->author}}</h5>
                        </div>
                        <p>{{$comment->body}}</p>
                    </div>
                </div>
            @endforeach
    </div>


@endsection
