@extends('layouts.app')


@section('content')

    <h1>Все категории</h1>
    <a href="{{route('admin.categories.create')}}" class="btn btn-primary">Добавить категорию</a>
    <a href="{{route('admin.products.index')}}"  class="btn btn-primary">Список продуктов</a>
    <br>
    <br>
    <table class="table">
        <thead class="table-dark">
        <tr>
            <th>#</th>
            <th>name</th>
            <th>action</th>
        </tr>
        </thead>
        <tbody>
       @foreach($categories as $category)
           <tr>
               <td>{{$category->id}}</td>
               <td>{{$category->name}}</td>
               <td>
                   <a href="{{route('admin.categories.edit' , ['category' => $category])}}" class="btn btn-primary">edit</a>
                   <br>
                   <br>
                   <form method="POST" action="{{route('admin.categories.destroy', ['category' => $category])}}">
                       @method('DELETE')@csrf
                       <button type="submit" class="btn btn-danger">delete</button>
                   </form>
               </td>
           </tr>
       @endforeach
        </tbody>
    </table>


@endsection
