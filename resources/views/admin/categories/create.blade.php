@extends('layouts.app')


@section('content')
    <h3>Создать категорию</h3>
    <form enctype="multipart/form-data" action="{{route('admin.categories.store')}}" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" id="name" name="name">
        </div>
        <button type="submit">Создать</button>
    </form>

@endsection
