@extends('layouts.app')


@section('content')
    <h3>Редактировать категорию</h3>
    <form enctype="multipart/form-data" action="{{route('admin.categories.update' , ['category' => $category])}}" method="POST">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" id="name" name="name" value="{{$category->name}}">
        </div>
        <button type="submit">Создать</button>
    </form>


@endsection
