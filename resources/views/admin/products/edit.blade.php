@extends('layouts.app')


@section('content')

    <div class="container">
        <h1>Изменить товар</h1>
        <div class="row">
            <form enctype="multipart/form-data" action="{{route('admin.products.update' ,['product' => $product])}}" method="POST">
                @method('PUT') @csrf
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" id="name" name="name" value="{{$product->name}}"/>
                </div>
                <div class="form-group">
                    <label for="price">Цена</label>
                    <input type="number" step=".01" id="price" name="price" value="{{$product->price}}">
                </div>
                <div class="form-group">
                    <select class="custom-select" name="category_id">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">@if($category == $product->category) selected  @endif  {{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="picture">Фото</label>
                    <input type="file" id="picture" name="picture" value="{{$product->picture}}">
                </div>
                <br>
                <button type="submit">Изменить</button>
            </form>
        </div>

    </div>



@endsection
