@extends('layouts.app')

@section('content')
    <div class="container-lg container-md container-sm container-xl">
        <a href="{{route('admin.products.create')}}" class="btn btn-primary">@lang('messages.create_new_products')</a>
        <a href="{{route('admin.categories.index')}}" class="btn btn-outline-primary">all categories</a>
        <br>
        <h1>@lang('messages.products_list')</h1>
        @include('stations.status')
        <table class="table">
            <thead class="table-dark">
            <tr>
                <th>#</th>
                <th>@lang('messages.name')</th>
                <th>@lang('messages.price')</th>
                <th>Category</th>
                <th>@lang('messages.picture')</th>
                <th>@lang('messages.action')</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{$product->id}}</td>
                    <td><a href="{{route('admin.products.show' ,['product' => $product])}}">{{$product->name}}</a></td>
                    <td>{{$product->price}} $</td>
                    <td>@if(!is_null($product->category))
                        {{$product->category->name}}
                        @else
                            No category
                        @endif</td>
                    <td>
                        @if(!is_null($product->picture))
                            <img width="50px" height="50px" src="{{asset('/storage/' . $product->picture)}}" alt="{{$product->picture}}">
                        @else
                            <p>not picture</p>
                        @endif
                    </td>
                    <td>
                        <a href="{{route('admin.products.edit' , ['product' => $product])}}" class="btn btn-primary">@lang('messages.edit')</a>
                        <br>
                        <br>
                        <form method="POST" action="{{route('admin.products.destroy' ,['product' => $product])}}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger">@lang('messages.delete')</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div>
            {{$products->links('pagination::bootstrap-4')}}
        </div>


    </div>


@endsection
