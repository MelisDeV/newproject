@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Добавить новый товар</h1>

    <div class="row">
        <form enctype="multipart/form-data" action="{{route('admin.products.store')}}" method="POST"> @csrf
            <div class="form-group">
                <label for="name">@lang('messages.name')</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name"/>
                @error('name')
                <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group">
                <label for="price">@lang('messages.price')</label>
                <input type="number" step=".01" id="price" class="form-control @error('price') is-invalid @enderror" name="price">
                @error('price')
                <p class="text-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group">
                <label for="description">description</label>
                <input type="text"  id="description" class="form-control" name="description">
            </div>
            <div class="form-group">
                <select class="custom-select" name="category_id">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="picture">@lang('messages.picture')</label>
                <input type="file" id="picture" name="picture">
            </div>
            <br>
            <button type="submit">Добавить</button>
        </form>
    </div>

</div>

@endsection
