@extends('layouts.app')


@section('content')

    <div class="cardShow" style="width: 18rem" >
        <img src="{{asset('/storage/' .$product->picture)}}" alt="{{$product->picture}}}" class="card-img-top" alt="{{$product->picture}}">
        <div class="card-body">
            <h5 class="card-title">{{$product->name}}</h5>
            <p>Цена: {{$product->price}} $</p>
            <p>Описание:{{$product->description}}</p>
            @if(!is_null($product->category))
                <h4>Категория:{{$product->category->name}}</h4>
            @else
                <h4>No category</h4>
            @endif
            <h4><b>Comments</b></h4>
            <div class="block">
                @foreach($product->comments as $comment)
                    <h1>Author: {{$comment->author}}</h1>
                    <p>Comment: {{$comment->body}}</p>
                @endforeach
            </div>
            <a href="{{route('admin.products.index')}}" class="btn btn-primary">@lang('messages.back')</a>
        </div>
    </div>
@endsection
