<?php


namespace App\Http\Form;

use Illuminate\Http\Request;


abstract class Form
{
 public static function execute(Request $request)
 {
     return (new static)->handle($request);
 }

   protected abstract function handle(Request $request);
}
