<?php


namespace App\Http\Form;


use App\Models\Product;
use Illuminate\Http\Request;

class ProductForm extends Form
{

    protected function handle(Request $request)
    {
        $data = $request->all();
        $file = $request->file('picture');
        if(!is_null($file)){
            $path = $file->store('pictures','public');
            $data['picture'] = $path;
        }

        return Product::create($data);
    }
}
