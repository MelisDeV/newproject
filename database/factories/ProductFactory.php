<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $ru_faker = \Faker\Factory::create('ru_RU');
        return [
            'name' => $this->faker->name(),
            'price' => rand(200, 1000) ,
            'picture' => $this->getImage(rand(1,4)),
            'category_id' => rand(1,3),
            'description' => $this->faker->text
        ];
    }


    private function getImage($image_number = 1):string
    {
        $path = storage_path() ."/product_picture/" .$image_number . ".jpg";
        $image_name = md5($path) .".jpg";
        $resize = \Intervention\Image\Facades\Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('pictures/' .$image_name, $resize->__toString());
        return 'pictures/' .$image_name;
    }
}
